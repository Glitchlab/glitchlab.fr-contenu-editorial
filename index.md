---
title: Bienvenue au Glitchlab
description: Glitchlab est un makerspace participatif à Rouen. Prenez part à l'aventure en rejoignant notre communauté de Makers !
punchline: Inventez, créez, innovez, apprenez, rencontrez
---

# Le Glitchlab
[Session Open bidouille au Glitchlab](images/session-open-bidouille.jpg)
Session Open Bidouille © Josselin 2018

Glitchlab est un makerspace participatif à Rouen. Prenez part à l'aventure en rejoignant notre communauté de Makers !
