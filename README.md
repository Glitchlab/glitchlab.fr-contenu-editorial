# Gestion du contenu éditorial de Glitchlab.fr

Vous pouvez contribuer au contenu via [le dépôt Gitlab du Glitchlab](https://framagit.org/Glitchlab/glitchlab.fr-contenu-editorial). Vous pouvez **modifier un fichier** existant en affichant le fichier en question, puis en cliquant sur le bouton "Edit". Vous pouvez **ajouter un fichier** cliquanr sur le bouton "[+]" depuis l'[interface principale du dépôt](https://framagit.org/Glitchlab/glitchlab.fr-contenu-editorial).

Des requêtes peuvent êtres émises sur [le gestionnaire de ticket](https://framagit.org/Glitchlab/glitchlab.fr-contenu-editorial/issues) ([documentation sur le gestionnaire de ticket](https://framagit.org/help/user/project/issues/index.md#issue-tracker)).
