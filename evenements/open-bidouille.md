---
title: Open bidouille
description: Venez bidouiller, découvrir, participer, rigoler…"
cover:
  src: evenements/images/life-is-a-glitch.jpg
  alt: Life is a Glitch
date: Tous les mercredis à partir de 18h30
place: "
[Au café couture](http://www.aucafecouture.fr/)
7, rue Alsace-Lorraine,
76000 Rouen
"
more-link-text: Plus d'informations sur les Open bidouilles
---

# Les Open bidouilles du Glithlab

<!--
@todo : ajouter de chatoyants textes & images
@ticket: #?
-->
