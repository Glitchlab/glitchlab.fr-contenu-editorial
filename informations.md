---
title: Informations
# En complément de cette sous-navigation :
#  - les identifiants d'ancres pourrons
#    être générés via un header identifier
#  - la sous-navigation peut être remplacer par
#    un générateur de T.O.C.
navigation:
  a-propos-du-glitchlab: À propos
  participer-au-glitchlab: Particper
  rencontrer-le-glitchlab: Nous rencontrer
  contacter-le-glitchlab: Nous contacter
---


# À propos du Glitchlab

Un *glitch* est une défaillance d'un matériel électronique ou d'un logiciel informatique provoquant un comportement inattendu. Ce *bug* permet dans certains jeux vidéo d'arriver plus rapidement à la fin du jeu. Imprévisible mais souvent bienvenu.

Partant de ce principe, le lab a pour ambition de créer **un atelier ouvert, participatif et collaboratif** (autrement appelé [makerspace](https://fr.wikipedia.org/wiki/Makerspace)), un lieu réunissant outils, machines et savoir-faire dans une multitude de domaines où chacun pourra venir réaliser des projets propres ou communs, fabriquer, réparer, apprendre…

L'association Glitchlab réunit [une communauté de Makers](https://fr.wikipedia.org/wiki/Culture_maker) qui partagent une passion pour le « fait à la main », le DIY ([Do It Yourself](https://fr.wikipedia.org/wiki/Do_it_yourself)). Cette communauté se veut ouverte et ne demande qu'à s'agrandir ! Nul besoin d'être spécialiste ou pétri de connaissance, l'envie suffit (et un peu de temps :) ) pour commencer à réaliser ses projets. Venez nous rencontrer, apprendre et partager vos connaissances !

## Participer au Glitlab

Chaque semaine, la communauté du Glitch Lab se retrouve afin d'échanger et de partager autour des projets de chacun ou des projets en commun.

Venez participer, les rencontres ont lieu **tous les mercredis à partir de 18h au [Café Couture](http://aucafecouturerouen.fr) ou le jeudi à partir de 19h à [Guidoline](http://www.guidoline.com)**

Des soirées Openbidouille on lieu au Café Couture tous les mercredis et un jeudi par mois, à partir de 18h.

### Pour aller plus loin

Quelques exemples de réalisations des membres du Glitchlab et de l'ambiance de folle bidouille qui y règne sont rassemblés sur la page des [journées portes ouvertes](evenements/journees-portes-ouvertes).

[Le wiki du Glitchlab](https://wiki.glitchlab.fr) est à dispoition pour documenter les projets et construire une base de connaissances accessible.

Également, n'hésitez pas à participer aux discussions sur [le forum du Glitchlab](https://forum.glitchlab.fr/).

## Rencontrer le Glitchlab

Nous sommes présent [Au café couture](http://aucaecouturerouen.fr) **tous les mercredis à partir de 18h00**, ou a [Guidoline](http://www.guidoline.fr/) **les jeudis à partir de 19h00**.

## Contacter le Glitlab

Pour toute questions ou remarque, ou simplement pour nous passer le bonjour, n'hésitez-pas à nous contacter sur notre courriel hello@glitchlab.fr
