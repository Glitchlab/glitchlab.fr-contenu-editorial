---
# @todo : Voir pour ajouter l'année courante automatiqument {{current-year}}
# @repository:
# @ticket: #?
title: Les évenements 2019 du Glitchlab
description: Les évenements de l'année 2019 du Glitchlab.
# La navigation sera généré automatiquement
# à l'aide d'une collection.
navigation:
 evenements-recurrents: Évenements récurents
 janvier: Janvier
 février: Février
 mars: Mars
---

# Évenements 2019
Les évenements de l'année 2018 du Glitchlab.

<!--
La grande partie du contenu de cette page est généré automatiquement à partir des fichiers du répertoire `evenements`.

Il est néanmoins possible d'ajouter un peut de contenu complémentaire (note, information, etc.) ci-dessous.
-->

